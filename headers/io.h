#ifndef IO_H
#define IO_H

#include "vector.h"

dyn_vector *read(char *filename, int param);

#endif
