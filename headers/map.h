#ifndef MAP_H
#define MAP_H

int *create_ranges(int n_words, int n_proc);
int *create_sizes(int *ranges, int n);
int *create_char_ranges(int *int_arr, int *ranges, int n_proc);
int *create_char_sizes(int *int_arr, int *sizes, int n_proc);
int *create_indexes(int *sizes, int n_proc);
/* napawia indeksy w int_arr po rozeslaniu do innych procesow */
void fix_words_array(int *t, int size);
/* napawia indeksy w int_arr po odebraniu od innych procesow */
void fix_int_arr(int *int_arr, int *word_count, int *words_index, int summary_size);
/* odpowiednio przesuwa indeksy utworzonych przez gather tablic, jest to potrzebne do gatterv*/
void adjust_gather_arr(int *arr, int *sizes, int size);

#endif
