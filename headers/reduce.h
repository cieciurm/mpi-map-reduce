#ifndef REDUCE_H
#define REDUCE_H

#include "vector.h"

dyn_vector *reduce(char *char_arr, int *int_arr, int *count_arr, int n_char, int n_words);
void add_word(dyn_vector *vect, char *word, int counter);
char *get_word_from_arr(char *char_arr, int *int_arr, int n_char, int n_words, int index);
void print_result(dyn_vector *vect);
#endif
