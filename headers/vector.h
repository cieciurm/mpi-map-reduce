#ifndef VECTOR_H
#define VECTOR_H

#define INIT_SIZE 32

typedef struct dyn_vector {
	char *char_arr;
	int char_size;
	int char_n_words; /* liczba slow */
	int char_n; /* liczba znakow */

	int *int_arr; /* indeksy kolejnych slow */
	int int_size;
	int int_n;
	int *count_arr; /* liczba wystapien */
} dyn_vector;

/*typedef struct int_vector {*/
/*int *arr;*/
/*int size;*/
/*int n;*/
/*} int_vector;*/

dyn_vector *dyn_vector_init(); 

int dyn_vector_add(dyn_vector *vect, char *elem);

char *dyn_vector_char_truncate(dyn_vector *vect);

int *dyn_vector_int_truncate(dyn_vector *vect);

void dyn_vector_print(dyn_vector *vect);

void dyn_vector_free(dyn_vector *vect);

/*int_vector *init_int_dynamic(); */

/*int add_int_dynamic(int_vector *arr, int elem);*/

/*char *trunc_int_arr(int_vector *s);*/

/*void print_int_dynamic(int_vector *vect);*/

#endif
