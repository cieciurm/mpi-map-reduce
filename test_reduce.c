#include <stdio.h>
#include <stdlib.h>

#include "vector.h"
#include "reduce.h"
#include "io.h"

int main(int argc, char **argv) {
	dyn_vector *vect, *reduced_vect;
	char *word;
	int i;

	vect = read(argv[1], 0);

	dyn_vector_print(vect);
	
	reduced_vect = reduce(vect->char_arr, vect->int_arr, vect->char_n, vect->char_n_words);
	printf("Reduced vector:\n");
	dyn_vector_print(reduced_vect);

	
	dyn_vector_free(vect);

	return 0;
}
