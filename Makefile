CC=cc
mpiCC=mpicc

HEADER_OPT=-Iheaders

CC_OPT=-DDEBUG
IO_CC_OPT=-DIODEBUG

TESTS=test_io test_vector test_map test_mpi

vector: vector.c test_vector.c
	$(CC) $(HEADER_OPT) $? -o test_vector

io: io.c test_io.c vector.c
	$(CC) $(HEADER_OPT) $(IO_CC_OPT) $? -o test_io

map: io.c map.c vector.c test_map.c
	$(CC) $(HEADER_OPT) $? -o test_map

mpi: io.c map.c vector.c reduce.c arrays.c mpi.c
	$(mpiCC) $(HEADER_OPT) $? -o test_mpi

mpi_old: io.c map.c vector.c reduce.c test_mpi.c
	$(mpiCC) $(HEADER_OPT) $? -o test_mpi

reduce: io.c map.c vector.c reduce.c test_reduce.c
	$(CC) $(HEADER_OPT) $? -o test_reduce

clean:
	rm -f $(TESTS)
