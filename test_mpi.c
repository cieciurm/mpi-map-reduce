#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "io.h"
#include "map.h"

void print(int *t, int size) {
	int i;

	for (i = 0; i < size; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

void print_char(char *t, int size) {
	int i;

	for (i = 0; i < size; i++)
		printf("[%c]", t[i]);
	printf("\n");
}

int main(int argc, char **argv) {

	int n_proc, current_proc, i, s, e, local_n_words, local_n_char;
	dyn_vector *vect, *local_vect;
	int *sizes, *ranges, *char_ranges, *local_sizes, *char_sizes, *indexes;
	char *local_char_arr;
	int *local_int_arr; 
	int *local_fixed_int_arr; /* przesuniete indeksy do 0 */
	char *words; 
	int *char_sizes_2;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &current_proc);

	vect = dyn_vector_init();
	vect = read("logs/lolo", 0);

	if (current_proc == 0) {
		ranges = create_ranges(vect->int_n, n_proc);
		sizes = create_sizes(ranges, n_proc);			
		char_sizes = create_char_sizes(vect->int_arr, sizes, n_proc);
		char_ranges = create_char_ranges(vect->int_arr, ranges, n_proc);
		indexes = create_indexes(sizes, n_proc);

		printf("Ranges:\n");
		print(ranges, n_proc);
		printf("Sizes:\n");
		print(sizes, n_proc);
		printf("Char sizes:\n");
		print(char_sizes, n_proc);
		printf("Char ranges:\n");
		print(char_ranges, n_proc);
		printf("Indexes ranges:\n");
		print(indexes, n_proc);

		dyn_vector_print(vect);
	}

	/*rozsylamy ilosc znakow do pobrania przez procesy*/
	MPI_Scatter(char_sizes, 1, MPI_INT, &local_n_char, 1, MPI_INT, 0, MPI_COMM_WORLD);

	/*rozsylamy ilosc slow do pobrania przez procesy*/
	MPI_Scatter(sizes, 1, MPI_INT, &local_n_words, 1, MPI_INT, 0, MPI_COMM_WORLD);
	
	local_char_arr = malloc(local_n_char * sizeof(char));
	local_int_arr = malloc(local_n_words * sizeof(int));

	/*rozsylamy tablice charow*/
	MPI_Scatterv(vect->char_arr, char_sizes, char_ranges, MPI_CHAR, local_char_arr, 100, MPI_CHAR, 0, MPI_COMM_WORLD);

	/*rozsylamy tablice int*/
	MPI_Scatterv(vect->int_arr, sizes, indexes, MPI_INT, local_int_arr, 100, MPI_INT, 0, MPI_COMM_WORLD);

	/*naprawiamy przesuniecie indexow*/
	local_fixed_int_arr = fix_words_array(local_int_arr, local_n_words);

	/*printf("rank #%d : liczba slow: %d\n", current_proc, local_n_words);*/
	printf("rank #%d, local_n_char: %d, local_char_arr:\n", current_proc, local_n_char);
	print_char(local_char_arr, local_n_char);
	print(local_fixed_int_arr, local_n_words);

	/*GATHER*/

	words = malloc(vect->char_n * sizeof(char));

	MPI_Gatherv(local_char_arr, local_n_char, MPI_CHAR, 
	            words, char_sizes, char_ranges, MPI_CHAR, 
	            0, MPI_COMM_WORLD);

	if (current_proc == 0) {
		printf("Wielkosc tablicy do ktorej wysylamy: %d\n", vect->char_n);
		printf("[gather]----\n");
		print_char(words, vect->char_n);
	}

	MPI_Finalize();

	return 0;
}
