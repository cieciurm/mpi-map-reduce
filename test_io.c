#include <stdio.h>
#include <stdlib.h>

#include "io.h"
#include "vector.h"

int main() {
	dyn_vector *vect;

	vect = read("logs/minilog", 0);

	dyn_vector_print(vect);

	dyn_vector_free(vect);
	return 0;
}
