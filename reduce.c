#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "reduce.h"
#include "vector.h"


dyn_vector *reduce(char *char_arr, int *int_arr, int *count_arr, int char_n, int n_words) {

	dyn_vector *vect;
	int i, counter;
	char *tmp_word;

	vect = dyn_vector_init();
	vect->count_arr = malloc(INIT_SIZE * sizeof(int));

	for (i = 0; i < n_words; i++){
		if(count_arr == NULL)
			counter = 1;
		else 
			counter = count_arr[i];
			
		// wyciagamy wszystkie slowa pokolei i dodajemy je do vect
		tmp_word = get_word_from_arr(char_arr, int_arr, char_n, n_words, i);			
		add_word(vect, tmp_word, counter);
	}
		

	return vect;
}

void add_word(dyn_vector *vect, char *word, int counter) {
	int i;
	char *tmp_word;
	
	for (i = 0; i < vect->char_n_words; i++) {
		tmp_word = get_word_from_arr(vect->char_arr, vect->int_arr, vect->char_n, vect->char_n_words, i);
		if (strcmp(word, tmp_word) == 0) { 	
			vect->count_arr[i] += counter;
			//printf("Zwiekszylem licznik!");			
			return;
		}
	}

	/* jak dojechalismy tutaj, to znaczy ze nie ma takeigo slowa */
	dyn_vector_add(vect, word);
	vect->count_arr[i] = counter;
	//printf("Dodalem slowo!\n");	
}

char *get_word_from_arr(char *char_arr, int *int_arr, int char_n, int char_n_words, int index){
	int i, size;
	char *result;

	/* ostatnie slowo */
	if (index == (char_n_words - 1))
		size = char_n - int_arr[index];
	else 
		size = int_arr[index + 1] - int_arr[index];

	//printf("index=%d, size=%d\n", index, size);
	result = malloc((size+1) * sizeof(char));
	memcpy(result, (char_arr+int_arr[index]), size * sizeof(char));
	result[size] = '\0';

	return result;
}

void print_result(dyn_vector *vect){
	int i;	
	printf("Wynik dzialania: \n");
	for(i = 0; i< vect->int_n; i++){
		printf("%d. %s\t %d\n", i, get_word_from_arr(vect->char_arr, vect->int_arr, vect->char_n, vect->int_n, i), vect->count_arr[i]);
	}
		
}
