#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "vector.h"

dyn_vector *dyn_vector_init() {
	dyn_vector *vect;

	vect = malloc(sizeof(dyn_vector));

	vect->char_size = INIT_SIZE;
	vect->char_n = 0;
	vect->char_n_words = 0;
	vect->char_arr = malloc(vect->char_size * sizeof(char));

	vect->int_size = INIT_SIZE;
	vect->int_n = 0;
	vect->int_arr = malloc(vect->int_size * sizeof(int));
	vect->count_arr = malloc(vect->int_size * sizeof(int));

	return vect;
}

int dyn_vector_add(dyn_vector *vect, char *elem) {
	int elem_len, next_word_index;

	elem_len = strlen(elem);

	if ((vect->char_n + elem_len) > vect->char_size) {
		vect->char_size *= 2;
		vect->char_arr = realloc(vect->char_arr, vect->char_size * sizeof(char));
	}

	if (vect->int_n == vect->int_size) {
		vect->int_size *= 2;
		vect->int_arr = realloc(vect->int_arr, vect->int_size * sizeof(int));
		vect->count_arr = realloc(vect->count_arr, vect->int_size * sizeof(int));
	}

	if (vect->char_n == 0)
		vect->int_arr[0] = 0;

	vect->int_arr[vect->int_n+1] = vect->int_arr[vect->int_n] + elem_len;
	vect->int_n++;
	
	next_word_index = vect->int_arr[vect->int_n-1];

#ifdef DEBUG
	printf("vect->int_n=%d, next_word_index=%d\n", vect->int_n, next_word_index);
#endif

	strcpy((vect->char_arr+next_word_index), elem);
	
	
	vect->count_arr[vect->char_n_words] = 1;
	vect->char_n_words++;
	vect->char_n += elem_len;

	

#ifdef DEBUG
	printf("Wkleilem slowo o dlugosci: %d\n", elem_len);
#endif

	return 0;
}

void dyn_vector_print(dyn_vector *vect) {
	int i, n_char;

	printf("[CHAR]\nsize: %d, n_words: %d, n: %d\n", vect->char_size, vect->char_n_words, vect->char_n);

	if (vect->char_n == 0) {
		printf("[PUSTY]\n");
	} else {
		for (i = 0; i < vect->char_n; i++)
			printf("[%c]", vect->char_arr[i]);
		printf("\n");
	}

	printf("[INT]\nsize: %d, n: %d\n", vect->int_size, vect->int_n);

	if (vect->int_n == 0) {
		printf("[PUSTY]\n");
	} else {
		for (i = 0; i < vect->int_n; i++) 
			printf("[%d]", vect->int_arr[i]);
		printf("\n");
	}

	printf("[COUNT]\nsize: %d, n: %d\n", vect->int_size, vect->int_n);
	if (vect->int_n == 0) {
		printf("[PUSTY]\n");
	} else {
		for (i = 0; i < vect->int_n; i++) 
			printf("[%d]", vect->count_arr[i]);
		printf("\n");
	}
}

void dyn_vector_free(dyn_vector *vect) {
	free(vect->char_arr);
	free(vect->int_arr);
	free(vect->count_arr);
	free(vect);
}


