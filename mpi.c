#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "io.h"
#include "map.h"
#include "reduce.h"
#include "arrays.h"


int main(int argc, char **argv) {
	int *char_tmp, *int_tmp;
	int n_proc, current_proc, i, s, e, local_n_words, local_n_char;
	dyn_vector *vect, *local_vect, *final_vect;
	int *sizes, *ranges, *char_ranges, *local_sizes, *char_sizes, *indexes, *final_char_sizes, *final_int_sizes;
	char *local_char_arr;
	int *local_int_arr; 
	int *local_fixed_int_arr; /* przesuniete indeksy do 0 */
	char *words; 

	if(argc < 2) {
		printf("Podaj plik wejsciowy!\n");
		return 0;
	}

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &current_proc);


	vect = dyn_vector_init();
	
	if (current_proc == 0) {
		vect = read(argv[1], 0);
		ranges = create_ranges(vect->int_n, n_proc);
		sizes = create_sizes(ranges, n_proc);			
		char_sizes = create_char_sizes(vect->int_arr, sizes, n_proc);		
		char_ranges = create_char_ranges(vect->int_arr, ranges, n_proc);		
		indexes = create_indexes(sizes, n_proc);
		
		printf("WEJSCIE:\n");
		dyn_vector_print(vect);
	}

	/*rozsylamy ilosc znakow do pobrania przez procesy*/
	MPI_Scatter(char_sizes, 1, MPI_INT, &local_n_char, 1, MPI_INT, 0, MPI_COMM_WORLD);

	/*rozsylamy ilosc slow do pobrania przez procesy*/
	MPI_Scatter(sizes, 1, MPI_INT, &local_n_words, 1, MPI_INT, 0, MPI_COMM_WORLD);
	
	local_char_arr = malloc(local_n_char * sizeof(char));
	local_int_arr = malloc(local_n_words * sizeof(int));

	/*rozsylamy tablice charow*/
	MPI_Scatterv(vect->char_arr, char_sizes, char_ranges, MPI_CHAR, local_char_arr, 100, MPI_CHAR, 0, MPI_COMM_WORLD);

	/*rozsylamy tablice int*/
	MPI_Scatterv(vect->int_arr, sizes, indexes, MPI_INT, local_int_arr, 100, MPI_INT, 0, MPI_COMM_WORLD);

	/* naprawiamy przesuniecie indexow */

	fix_words_array(local_int_arr, local_n_words);

	/*printf("rank #%d : liczba slow: %d\n", current_proc, local_n_words);*/


	/* REDUKCJA LOKALNA*/
	local_vect = reduce(local_char_arr, local_int_arr, NULL, local_n_char, local_n_words);
	

	/* GATHER */
	
	/*alokacja pamieci dla struktury i wektorow, do ktorych zbierane sa dane od procesow*/
	final_vect = malloc(sizeof(dyn_vector));
	
	if(current_proc == 0){
		int_tmp = malloc(n_proc * sizeof(int));
		char_tmp = malloc(n_proc * sizeof(int));		
		final_char_sizes = malloc(n_proc*sizeof(int));
		final_int_sizes = malloc(n_proc*sizeof(int));		
	}

	/* zlicza laczna ilosc odebranych znakow */
	MPI_Allreduce(&local_vect->char_n, &final_vect->char_n, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	
	/*Tworzy tablice z size'ami nowego wektora  <-- potrzebne to gatterv */
	MPI_Gather(&local_vect->char_n, 1, MPI_INT, final_char_sizes, 1, MPI_INT, 0, MPI_COMM_WORLD);
	
	/* zlicza laczna ilosc odebranych slow */
	MPI_Allreduce(&local_vect->int_n, &final_vect->int_n, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	
	/*Tworzy tablice z rangeami slow nowego wektora <-- potrzebne to gatterv */
	MPI_Gather(&local_vect->int_n, 1, MPI_INT, final_int_sizes,1, MPI_INT, 0, MPI_COMM_WORLD);


	/*	Ranges i sizes potrzebne do zebrania danych z tablic	*/
	if (current_proc == 0) {
		/*dostosowuje idneksy dla intow*/
		adjust_gather_arr(int_tmp, final_int_sizes, n_proc);
		
		/*dostosowuje idneksy dla charow*/
		adjust_gather_arr(char_tmp, final_char_sizes, n_proc);
		
		/* alokacja pamieci dla tablic z wynikami*/
		final_vect->char_arr = malloc(final_vect->char_n * sizeof(char));		
		final_vect->int_arr = malloc(final_vect->int_n * sizeof(int));
		final_vect->count_arr = malloc(final_vect->int_n * sizeof(int));								
	}

	/*Zbiera int_arr*/
	MPI_Gatherv(local_vect->int_arr, local_vect->int_n, MPI_INT, 
	            final_vect->int_arr, final_int_sizes, int_tmp, MPI_INT, 
	            0, MPI_COMM_WORLD);
	
	/*Zbiera count_arr*/
	MPI_Gatherv(local_vect->count_arr, local_vect->int_n, MPI_INT, 
	            final_vect->count_arr, final_int_sizes,  int_tmp, MPI_INT, 
	            0, MPI_COMM_WORLD);
	
	/*Zbiera count_arr*/
	MPI_Gatherv(local_vect->char_arr, local_vect->char_n, MPI_CHAR, 
	           final_vect->char_arr, final_char_sizes,  char_tmp, MPI_CHAR, 
	            0, MPI_COMM_WORLD);
	
	
	if (current_proc == 0) {		
		/*REDUKCJA GLOBALNA*/		
		fix_int_arr(final_vect->int_arr, final_int_sizes, char_tmp, n_proc);		
		vect = reduce(final_vect->char_arr, final_vect->int_arr, final_vect->count_arr, final_vect->char_n, final_vect->int_n);
			
		print_result(vect);
	}

	MPI_Finalize();

	return 0;
}
