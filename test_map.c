#include <stdio.h>
#include <stdlib.h>

#include "vector.h"
#include "io.h"
#include "map.h"

void print(int *t, int size) {
	int i;

	for (i = 0; i < size; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

int main(int argc, char** argv) {

	dyn_vector *vect;
	int i, n_proc, s, e, current_proc;
	int *ranges, *sizes;

	vect = read(argv[1], 0);
	n_proc = atoi(argv[2]);

	ranges = create_ranges(vect->int_n, n_proc);
	sizes = create_sizes(ranges, n_proc);

	dyn_vector_print(vect);

	printf("Ranges:\n");
	print(ranges, n_proc);
	printf("Sizes:\n");
	print(sizes, n_proc);

	for (i = 0; i < n_proc; i++) {	
		current_proc = i;

		if (current_proc == 0)
			s = 0;
		else
			s = ranges[current_proc-1];

		e = ranges[current_proc]-1;
		
		printf("#%d proces: s=%d, e=%d, size=%d\n", current_proc, s, e, e-s+1);
	}

	dyn_vector_free(vect);
	free(ranges);
	free(sizes);

	return 0;
}
