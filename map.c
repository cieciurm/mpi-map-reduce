#include <stdlib.h>
#include <stdio.h>

#include "map.h"

int *create_ranges(int n_points, int n_proc) {
	int *ranges, *steps;
	int step, remainder, i, tmp;

	ranges = malloc(n_proc * sizeof(int));
	steps = malloc(n_proc * sizeof(int));

	step = n_points / n_proc;
	remainder = n_points % n_proc;
	
	for (i = 0; i < n_proc; i++) {
		steps[i] = step;
		if (remainder > 0) {
			steps[i]++;
			remainder--;
		}
	}
	
	tmp = 0;

	for (i = 0; i < n_proc; i++) {
		tmp += steps[i];
		ranges[i] = tmp;
	}
	
	free(steps);
	return ranges;
}

int *create_sizes(int *ranges, int n) {
	int *sizes;
	int i;

	sizes = malloc(n * sizeof(int));

	for (i = 0; i < n; i++)
		if (i == 0)
			sizes[i] = ranges[i];
		else
			sizes[i] = ranges[i] - ranges[i-1];

	return sizes;
}

int *create_char_ranges(int *int_arr, int *ranges, int n_proc){
	int *indexes;	
	int i, j, tmp;

	indexes = malloc(n_proc*sizeof(int));
	
	indexes[0] = 0;
	
	for(i=0; i<n_proc-1; i++){		
		indexes[i+1] = int_arr[ranges[i]];
	}

	return indexes;
}

int *create_char_sizes(int *int_arr, int *sizes, int n_proc){
	int *local_sizes;	
	int i, j, tmp, counter;

	local_sizes = malloc(n_proc*sizeof(int));
		
	counter=0;

	for(i=0; i<n_proc; i++){
		tmp = 0;
		for(j=0; j<sizes[i]; j++){
			tmp += int_arr[counter+1] - int_arr[counter];
			counter++;		
		}
		local_sizes[i] = tmp;
	}
	return local_sizes;
}

int *create_indexes(int *sizes, int n_proc) {
	int i, total_sizes;
	int *indexes;

	indexes = malloc(n_proc * sizeof(int));
	total_sizes = 0;

	for (i = 0; i < n_proc; i++) {
		if (i == 0)
			indexes[i] = 0;
		else
			indexes[i] = total_sizes;

		total_sizes += sizes[i];
	}

	return indexes;
}



void fix_words_array(int *t, int size) {
	int i, first;

	first = t[0];

	if (first == 0)
		return;
	else
		for (i = 0; i < size; i++)
			t[i] -= first;

}

void fix_int_arr(int *int_arr, int *word_count, int *words_index, int summary_size){
	int i, j, counter;

	counter = 0;
	printf("Size: %d\n",summary_size);
	for(i=0; i< summary_size; i++)
		for(j = 0; j< word_count[i]; j++, counter++){
			int_arr[counter] += words_index[i];	
		}
	
	
}

void adjust_gather_arr(int *arr, int *sizes, int size){
	int i;	

	for(i=size-1; i>0; i--)	
		arr[i] = sizes[i-1];
	
	arr[0] = 0;
	
	for(i=1; i<size; i++)		
		arr[i] += arr[i-1];	
}
