#include <stdlib.h>
#include <stdio.h>

#include "arrays.h"

void print(int *t, int size) {
	int i;

	for (i = 0; i < size; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

void print_char(char *t, int size) {
	int i;

	for (i = 0; i < size; i++)
		printf("[%c]", t[i]);
	printf("\n");
}


