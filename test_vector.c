#include <stdio.h>
#include <stdlib.h>

#include "vector.h"

int main() {
	dyn_vector *vect;
	int i;
	char *s = "Ala";
	char *s2 = "Ola";
	char *s3 = "Ela";
	char *s4 = "Mateusz";
	
	vect = dyn_vector_init();

	printf("*****************************przed dodaniem\n");
	dyn_vector_print(vect);

	dyn_vector_add(vect, s);
	dyn_vector_add(vect, s2);
	dyn_vector_add(vect, s3);
	dyn_vector_add(vect, s4);
	dyn_vector_add(vect, s4);
	dyn_vector_add(vect, s4);
	dyn_vector_add(vect, s4);

	for (i = 0; i < 100; i++)
		dyn_vector_add(vect, s4);

	printf("*****************************po dodaniu\n");
	dyn_vector_print(vect);

	dyn_vector_free(vect);
	return 0;
}
