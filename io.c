#include <stdio.h>
#include <string.h>

#include "io.h"
#include "vector.h"

#define BUFF_SIZE 500

dyn_vector *read(char *filename, int param) {
	FILE *f;
	char buff[BUFF_SIZE], *token, delimiter[2] = " ";
	dyn_vector *vect;

	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	vect = dyn_vector_init();

	f = fopen(filename, "r");

	while (fgets(buff, BUFF_SIZE, f) != NULL) {
#ifdef IODEBUG
		printf("%s\n", buff);
#endif

		/*
		 * dla ulatwienia zycia na razie bierzemy zawsze pierwszy token 
		 * => adres IP	
		 */

		token = strtok(buff, delimiter);

		/*while (token != NULL) {*/
		/*printf("%s\n", token);*/
		/*token = strtok(NULL, delimiter);*/
		/*}*/

		dyn_vector_add(vect, token);
	}

	fclose(f);

	return vect;
}
